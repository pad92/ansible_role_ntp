# ntp

[![Build status](https://git.depad.fr/ansible/roles/ntp/badges/master/build.svg)](https://git.depad.fr/ansible/roles/ntp/commits/master)

set ntp daemon

## Requirements

none

## Role Variables

```
cts_role_ntp:
  servers:
    - { address: "fr.pool.ntp.org", type: "pool" }
    - { address: "192.168.0.1",     type: "server" }
  restrict:
    - "127.0.0.1"
    - "10.0.0.0/16"
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - ntp
```

